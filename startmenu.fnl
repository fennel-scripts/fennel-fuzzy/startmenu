#!/bin/env fennel
(local fennel (require :fennel))
(set fennel.path (.. fennel.path ";/home/erik/fennelbin/lib/fzf/?.fnl"))

(local inspect (require :inspect))


;;(local fzf (require :lib.fzf.fzf))
(local fzf (require :fzf))
(local borders fzf.borders)
(local presets fzf.presets)

(local module
	   {
		:exe "./startmenu.fnl"
		:path "/home/erik/git/projects/lisp/fennel/fennel-fuzzy/startMenu"
		})


(local tym
	   (table.concat
		[
		 "tym "
		 "--role startmenu"
		 "--width 50"
		 "--height 20"
	 (.. "--cwd '" module.path "'")
	 (.. "-e " module.exe)
		 ]
		" "))
(local keybinds
	   [
		{:key "enter" :act (.. "execute(" module.exe "{})") :desc "run selected entry"}
		{:key "q"  :act "abort" :desc "quit program"}
		]
	   )
(fn binds [bnds]
  (local kbs [])
  (each [_ key (ipairs bnds)]
	(table.insert kbs (.. key.key ":" key.act))
	)
  (table.concat kbs ",")
  )


(fn helpKeyBinds [bnds]
  (local kbs [])
  (each [_ key (ipairs bnds)]
	(table.insert
	 kbs
	 (values (.. "[\"" key.key "\"]") key.desc)
	 )
	)
  kbs
  )



(fn printNice [lines]
  (each [i v (ipairs lines)]
	(print "WIP")
	)
  )
;;(fn preview [])
(fn showbinds []
  (print (table.concat ["-" :key  :description] ","))
  (each [i key (ipairs keybinds)]
	(print (table.concat [i key.key key.desc ] ","))
  )
  )



(local cfg (fzf.show
			fzf.feeders.echo
			[
			 :loggoff
			 :refresh
			 ]
			(presets.selector
			 {:border borders.rounded
			  :dumb true
			  ;;:bind "enter:execute(startmenu {})"
			  :bind "enter:execute(./startmenu.fnl {}),q:abort,"
			  }
			 )
			)
	   )


(local action (or (. arg 1) "default"))

(fn run [cmd]
  (os.execute (.. cmd "&"))
  (os.execute (.. "notify-send 'info' '" cmd "'"))
  ;;(print cmd)
  )


(match action
  :help (showbinds)
  :refresh (run "exit" true)
  :show (run tym)
  :default	(print (. (fzf.capture cfg) :out))
  _ (print (. (fzf.capture cfg) :out))
  )
